// Copyright 2021 Heiko Schaefer <heiko@schaefer.name>
//
// This file is part of tb-openpgp-certs
// https://gitlab.com/hkos/tb-openpgp-certs
//
// SPDX-FileCopyrightText: 2021 Heiko Schaefer <heiko@schaefer.name>
// SPDX-License-Identifier: GPL-3.0-or-later

use anyhow::Result;
use diesel::prelude::*;
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::fs::OpenOptions;
use std::path::PathBuf;

use sequoia_openpgp::cert::CertParser;
use sequoia_openpgp::parse::PacketParser;
use sequoia_openpgp::parse::Parse;
use sequoia_openpgp::serialize::Serialize;
use sequoia_openpgp::Cert;

use openpgp_keylist::Key;

use crate::models::{AcceptanceDecision, AcceptanceEmail};
use crate::schema::{acceptance_decision, acceptance_email};
use std::collections::hash_map::Entry::{Occupied, Vacant};

const CERT_RING_FILE: &str = "pubring.gpg";
const OPENPGP_SQLITE_FILE: &str = "openpgp.sqlite";

#[derive(Debug)]
pub struct TbAcceptance {
    fpr: String,
    emails: Vec<String>,
    decision: Decision,
}

/// "Acceptance decision" about this certificate in Thunderbird.
///
/// Note: clicking "Not yet, maybe later" means that there will be no entry
/// in the Thunderbird "decision" database (it will be removed, if it was
/// set, before).
#[derive(Debug)]
pub enum Decision {
    /// This key is marked as "my own key" in Thunderbird.
    Personal,

    /// "No, reject this key."
    Rejected,

    /// "Yes, but I have not verified that it is the correct key."
    Unverified,

    /// "Yes, I've verified in person this key has the correct fingerprint."
    Verified,
}

impl From<Decision> for &str {
    fn from(d: Decision) -> Self {
        match d {
            Decision::Personal => "personal",
            Decision::Rejected => "rejected",
            Decision::Unverified => "unverified",
            Decision::Verified => "verified",
        }
    }
}

impl From<String> for Decision {
    fn from(s: String) -> Self {
        match s.as_str() {
            "personal" => Decision::Personal,
            "rejected" => Decision::Rejected,
            "unverified" => Decision::Unverified,
            "verified" => Decision::Verified,
            _ => panic!("unexpected decision value {}", s),
        }
    }
}

pub fn read_openpgp_public_keyring(path: &PathBuf) -> HashMap<String, Cert> {
    let mut path = path.clone();
    path.push(CERT_RING_FILE);

    let keyring =
        std::fs::read(path.to_str().unwrap()).expect("failed to load keyring");

    let certs = keyring_to_certs(&keyring).expect("failed to process keyring");

    certs
        .into_iter()
        .map(|c| (c.fingerprint().to_hex().to_ascii_lowercase(), c))
        .collect()
}

pub fn add_to_keyring(path: &PathBuf, store: &[(Cert, Key)]) -> Result<()> {
    let mut certs = read_openpgp_public_keyring(path);

    for (cert, _) in store {
        let fp = cert.fingerprint().to_hex().to_ascii_lowercase();

        match certs.entry(fp) {
            Occupied(mut o) => {
                // merge new cert data with existing cert data from TB keystore
                let updated = o.get().clone().merge_public(cert.clone())?;

                o.insert(updated);
            }
            Vacant(v) => {
                // insert cert as a new entry in the TB keystore
                v.insert(cert.clone());
            }
        }
    }

    // write updated keystore back to filesystem (copy, update, move)
    let mut dest = path.clone();
    dest.push(CERT_RING_FILE);

    let mut tmp_file = path.clone();
    tmp_file.push(format!("{}.tmp", CERT_RING_FILE));

    // preserve the permissions of the TB keyring file
    std::fs::copy(&dest, &tmp_file)?;

    let mut file = OpenOptions::new()
        .write(true)
        .truncate(true)
        .create(false)
        .open(&tmp_file)?;

    write_certs(&mut file, &certs.values().collect::<Vec<_>>())?;

    // move tmp file to final location
    std::fs::rename(tmp_file, dest)?;

    Ok(())
}

fn write_certs(
    mut output: &mut dyn std::io::Write,
    certs: &[&Cert],
) -> Result<()> {
    for cert in certs {
        cert.serialize(&mut output)?;
    }
    Ok(())
}

fn keyring_to_certs(keyring: &[u8]) -> Result<Vec<Cert>> {
    let ppr = PacketParser::from_bytes(keyring)?;

    let mut res = vec![];
    for cert in CertParser::from(ppr) {
        res.push(cert?);
    }

    Ok(res)
}

pub fn add_to_acceptance_db(
    path: &PathBuf,
    store: &[(Cert, Key)],
) -> Result<()> {
    let conn = open_acceptance_db(path);

    conn.transaction::<_, anyhow::Error, _>(|| {
        for (cert, key) in store {
            let decision: &str = Decision::Verified.into();

            let ad = AcceptanceDecision {
                fpr: cert.fingerprint().to_hex().to_ascii_lowercase(),
                decision: decision.to_string(),
            };

            // insert or update
            diesel::replace_into(acceptance_decision::table)
                .values(&ad)
                .execute(&conn)?;

            if let Some(email) = &key.email {
                let ae = AcceptanceEmail {
                    fpr: cert.fingerprint().to_hex().to_ascii_lowercase(),
                    email: email.to_string(),
                };

                diesel::insert_or_ignore_into(acceptance_email::table)
                    .values(&ae)
                    .execute(&conn)?;
            }
        }
        Ok(())
    })
}

fn get_decisions(conn: &SqliteConnection) -> Vec<AcceptanceDecision> {
    acceptance_decision::table
        .load::<AcceptanceDecision>(conn)
        .expect("Error loading acceptance table")
}

fn get_emails(conn: &SqliteConnection) -> Vec<AcceptanceEmail> {
    acceptance_email::table
        .load::<AcceptanceEmail>(conn)
        .expect("Error loading email table")
}

fn open_acceptance_db(path: &PathBuf) -> SqliteConnection {
    let mut path = path.clone();
    path.push(OPENPGP_SQLITE_FILE);

    SqliteConnection::establish(path.to_str().unwrap())
        .expect("Error opening sqlite database")
}

pub fn read_acceptance_db(path: &PathBuf) -> HashMap<String, TbAcceptance> {
    let conn = open_acceptance_db(path);

    let decisions = get_decisions(&conn);
    let emails = get_emails(&conn);

    let mut e: HashMap<String, Vec<_>> = HashMap::new();

    for email in emails {
        match e.entry(email.fpr) {
            Entry::Occupied(mut o) => o.get_mut().push(email.email),
            Entry::Vacant(v) => {
                let _ = v.insert(vec![email.email]);
            }
        }
    }

    let mut tbacc = HashMap::new();

    for d in decisions {
        let emails = e.get(&d.fpr).cloned().unwrap_or_else(Vec::new);

        tbacc.insert(
            d.fpr.clone().to_ascii_lowercase(),
            TbAcceptance {
                fpr: d.fpr,
                emails,
                decision: d.decision.into(),
            },
        );
    }

    tbacc
}
