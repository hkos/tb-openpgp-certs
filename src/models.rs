// Copyright 2021 Heiko Schaefer <heiko@schaefer.name>
//
// This file is part of tb-openpgp-certs
// https://gitlab.com/hkos/tb-openpgp-certs
//
// SPDX-FileCopyrightText: 2021 Heiko Schaefer <heiko@schaefer.name>
// SPDX-License-Identifier: GPL-3.0-or-later

use super::schema::*;

#[derive(Insertable, Queryable, Debug, Clone, AsChangeset)]
#[table_name = "acceptance_decision"]
pub struct AcceptanceDecision {
    pub fpr: String,
    pub decision: String,
}

#[derive(Insertable, Queryable, Debug, Clone, AsChangeset)]
#[table_name = "acceptance_email"]
pub struct AcceptanceEmail {
    pub fpr: String,
    pub email: String,
}
