// Copyright 2021 Heiko Schaefer <heiko@schaefer.name>
//
// This file is part of tb-openpgp-certs
// https://gitlab.com/hkos/tb-openpgp-certs
//
// SPDX-FileCopyrightText: 2021 Heiko Schaefer <heiko@schaefer.name>
// SPDX-License-Identifier: GPL-3.0-or-later

#[macro_use]
extern crate diesel;

mod certdata;
pub mod cli;
pub mod models;
mod schema;

use anyhow::{Context, Result};
use std::collections::HashSet;
use std::path::PathBuf;
use std::str::FromStr;
use structopt::StructOpt;
use tokio::runtime::Runtime;

use sequoia_net::{wkd, KeyServer, Policy};
use sequoia_openpgp::parse::stream::{
    DetachedVerifierBuilder, MessageLayer, MessageStructure, VerificationHelper,
};
use sequoia_openpgp::parse::Parse;
use sequoia_openpgp::policy::StandardPolicy;
use sequoia_openpgp::{Cert, Fingerprint, KeyHandle};

// For the gpgsync demo Keylist
// https://github.com/firstlookmedia/gpgsync/blob/develop/example-keylist/README.md
// the CA key from k.o.o is not usable because it is stripped.
const DEFAULT_KEYSERVER: &str = "hkps://keyserver.ubuntu.com/";

fn main() {
    let cli = cli::Cli::from_args();
    let path = cli::Cli::from_args().path;

    match cli.cmd {
        cli::Command::Inspect {} => inspect(path.into()),
        cli::Command::Import { cmd } => match cmd {
            cli::ImportCommand::Keylist { url, ca_fp } => import_keylist(
                url,
                ca_fp,
                &PathBuf::from_str(&path).expect("bad path"),
            )
            .expect("Error while importing Keylist"),
            cli::ImportCommand::OpenpgpCa {} => unimplemented!(),
        },
    }
}

fn import_keylist(
    keylist_url: String,
    ca_fp: String,
    tb_path: &PathBuf,
) -> Result<()> {
    let skl = openpgp_keylist::SignedKeylist::from_url(keylist_url)?;

    let uri = Some(DEFAULT_KEYSERVER);

    // get ca cert from keylist URI, or using defaults
    let ca = retrieve_cert_by_fpr(&ca_fp, None, uri)
        .context("Failed to retrieve CA certificate by fingerprint")?;

    // check detached signature
    let check = |data: &[u8], detached: &str| -> Result<bool> {
        check_detached(data, detached, ca)
    };

    let kl = skl.verify(Box::new(check))?;

    // collect latest cert data (from keyserver/wkd) for each keylist entry
    let mut store = Vec::new();

    for key in kl.keys {
        // use keyserver from this key, if set - otherwise from metadata
        let keyserver = match key.keyserver {
            Some(_) => &key.keyserver,
            None => &kl.metadata.keyserver,
        };

        // load cert from keyserver/wkd
        match retrieve_cert_by_fpr(
            &key.fingerprint,
            key.email.as_deref(),
            keyserver.as_deref(),
        ) {
            Ok(cert) => {
                println!(
                    "Loaded {} (from {})",
                    &key.fingerprint,
                    keyserver.as_deref().unwrap_or("default source")
                );
                store.push((cert, key));
            }
            Err(e) => println!(
                "WARN: failed to retrieve '{}' [{}]: {:?}",
                &key.email.unwrap_or_else(|| "".to_string()),
                &key.fingerprint,
                e
            ),
        }
    }

    certdata::add_to_keyring(tb_path, &store)
        .context("Failed to add new certs to TB certring")?;
    certdata::add_to_acceptance_db(tb_path, &store)
        .context("Failed to add new certs to TB acceptance DB")?;

    println!("Certs and acceptance information stored in TB databases");

    Ok(())
}

/// Get a Cert for the fingerprint `fp`.
///
/// If `uri` is set, then only this specified Keyserver is queried.
///
/// If `uri` is not set:
/// 1) Wkd is queried (by email) - if the requested fingerprint is found
/// this way, then that Cert is used.
/// 2) If no matching Cert is found via Wkd, the default keyserver is
/// queried (by fingerprint)
fn retrieve_cert_by_fpr(
    fp: &str,
    email: Option<&str>,
    keyserver_uri: Option<&str>,
) -> Result<Cert> {
    let fp: Fingerprint = fp.parse()?;

    let p = Policy::Encrypted;

    // Pick keyserver, and/or try Wkd
    let mut ks = if let Some(uri) = keyserver_uri {
        // use specified keyserver!
        KeyServer::new(p, uri)?
    } else {
        if let Some(email) = email {
            // Try wkd first, use if a matching Cert is found
            if let Ok(Some(cert)) = find_via_wkd(&fp, email) {
                return Ok(cert);
            }
        }

        // ... then fall back to hagrid (if no result from wkd)
        KeyServer::keys_openpgp_org(p)?
    };

    // Actually query Keyserver
    Runtime::new()?.block_on(async move { ks.get(&fp).await })
}

/// Lookup `email` via Wkd.
///
/// If a cert is found that matches `fp`, return that Cert. Otherwise None.
fn find_via_wkd(fp: &Fingerprint, email: &str) -> Result<Option<Cert>> {
    let certs = Runtime::new()?.block_on(async move { wkd::get(email).await });

    Ok(certs?.iter().find(|&c| c.fingerprint() == *fp).cloned())
}

/// Check `data` against the armored `detached` signature, using `cert`.
fn check_detached(data: &[u8], detached: &str, cert: Cert) -> Result<bool> {
    struct Helper<'a>(&'a Cert);
    impl<'a> VerificationHelper for Helper<'a> {
        fn get_certs(&mut self, _: &[KeyHandle]) -> Result<Vec<Cert>> {
            Ok(vec![self.0.clone()])
        }

        fn check(&mut self, structure: MessageStructure) -> Result<()> {
            if let MessageLayer::SignatureGroup { ref results } =
                structure.iter().next().unwrap()
            {
                results.get(0).unwrap().as_ref().unwrap();
                Ok(())
            } else {
                panic!()
            }
        }
    }

    let p = StandardPolicy::new();

    let mut verifier = DetachedVerifierBuilder::from_bytes(
        detached.as_bytes(),
    )?
    .with_policy(&p, None, Helper(&cert))?;

    match verifier.verify_bytes(data) {
        Ok(_) => Ok(true),
        Err(e) => Err(e),
    }
}

fn inspect(path: PathBuf) {
    println!("Data from Thunderbird path '{:?}'", path);

    let acc = certdata::read_acceptance_db(&path);
    println!("Acceptance information {:#?}", acc);

    let certs = certdata::read_openpgp_public_keyring(&path);
    println!("Certificates {:#?}", certs);

    let acc_keys = acc.keys().collect::<HashSet<&String>>();
    let cert_keys = certs.keys().collect::<HashSet<&String>>();
    assert!(cert_keys.is_superset(&acc_keys));
}
