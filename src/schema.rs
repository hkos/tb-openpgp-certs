table! {
    acceptance_decision (fpr) {
        fpr -> Text,
        decision -> Text,
    }
}

table! {
    acceptance_email (fpr, email) {
        fpr -> Text,
        email -> Text,
    }
}
