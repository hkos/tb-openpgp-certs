// Copyright 2021 Heiko Schaefer <heiko@schaefer.name>
//
// This file is part of tb-openpgp-certs
// https://gitlab.com/hkos/tb-openpgp-certs
//
// SPDX-FileCopyrightText: 2021 Heiko Schaefer <heiko@schaefer.name>
// SPDX-License-Identifier: GPL-3.0-or-later

use clap::AppSettings;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "tb-openpgp-certs",
author = "Heiko Schäfer <heiko@schaefer.name>",
global_settings(& [AppSettings::VersionlessSubcommands,
AppSettings::DisableHelpSubcommand, AppSettings::DeriveDisplayOrder]),
about = "tb-openpgp-certs is a tool for inspecting and managing OpenPGP keys \
in Thunderbird 78+."
)]
pub struct Cli {
    #[structopt(name = "path", short = "p", long = "path")]
    pub path: String,

    #[structopt(subcommand)]
    pub cmd: Command,
}

#[derive(StructOpt, Debug)]
pub enum Command {
    /// Inspect Database
    Inspect {},
    /// Manage Users
    Import {
        #[structopt(subcommand)]
        cmd: ImportCommand,
    },
}

#[derive(StructOpt, Debug)]
pub enum ImportCommand {
    /// Import from OpenPGP CA
    OpenpgpCa {},

    /// Import from KeyList
    Keylist {
        url: String,

        #[structopt(name = "ca_fp", short = "f", long = "ca")]
        ca_fp: String,
    },
}
