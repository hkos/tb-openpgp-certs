<!--
SPDX-FileCopyrightText: 2021 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

This is a tool to interact with TB78 (and later) OpenPGP cert storage.
It only deals with "public keys", private keys are not handled.

CAUTION:
This tool is a hack to interact with Thunderbird's OpenPGP
certificate storage. It might eat your keyring ([see below](#details)).

# Import all keys from a Keylist into Thunderbird

Three parameters are needed to import a Keylist into the Thunderbird 
databases:

- The filesystem path of the 
  [Thunderbird profile](https://support.mozilla.org/en-US/kb/profiles-where-thunderbird-stores-user-data)
  that the Keylist should be imported into,
- the URL of a Keylist,
- the expected CA Fingerprint for that Keylist.

The Gpgsync
[demo Keylist](https://github.com/firstlookmedia/gpgsync/blob/develop/example-keylist/README.md)
can be imported into Thunderbird like this:

```
$ tb-openpgp-certs -p ~/.thunderbird/foobar.default/ import keylist https://raw.githubusercontent.com/firstlookmedia/gpgsync/develop/example-keylist/keylist.json --ca 4CA5857F960C8A78D82C11F36D00387A7A0206E2
```

# Details

Thunderbird 78+ stores public OpenPGP certificate data in two locations, 
in Thunderbird profile directories:

- `pubring.gpg` contains the actual OpenPGP certificates
- `openpgp.sqlite` contains some metadata, in particular the "acceptance" 
  level of each certificate

This tool modifies the content of those two files.

However, note that Thunderbird does not currently reload changes from these 
files until it is restarted.

Before running this tool, make backups of (at least) the files
`pubring.gpg` and `openpgp.sqlite`, if you worry about corrupting or losing 
your Thunderbird OpenPGP certificate store.
